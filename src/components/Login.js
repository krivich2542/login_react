
import React,{useRef,useState} from "react"
import {Button,Form,Card,Alert} from "react-bootstrap"
import {useAuth} from "../Authcontex/Authcontext"
import { Link,useNavigate } from "react-router-dom";




export default function Login() {
    const EmailRef = useRef();
    const PasswordRef = useRef();
    const { login } = useAuth();
    const [error,setError] = useState("")
    const [loading,setLoading] = useState(false)
    const navigate = useNavigate();

   async function handleSubmit(e){
        e.preventDefault()
       
       
        try{
            setError("")
            setLoading(true)
            await login(EmailRef.current.value, PasswordRef.current.value)
            navigate("/dashboard");
        }catch{
            setError("Failed To Log In")
        }
        setLoading(false)
    }

  return (
    <>
    <Card>
       <Card.Body>
        <h2 className='text-center mt-4'>Log In</h2>
       
        {error && <Alert variant='danger'>{error}</Alert>}
        <Form onSubmit={handleSubmit}>
            <Form.Group id='email'>
                <Form.Label>Email</Form.Label>
                <Form.Control type='email' ref={EmailRef} required/>
            </Form.Group>

            <Form.Group id='password'>
                <Form.Label>Password</Form.Label>
                <Form.Control type='password' ref={PasswordRef} required/>
            </Form.Group>
           
            
            
            <Button disabled={loading} className='w-100' type='submit'>Log in</Button>
        </Form>
       
       </Card.Body>

    </Card>
     <div className='w-100 text-center mt-2'>
        Don't have an account?  <Link to='/'>Sign up </Link>
     </div>
    </>
   
  )
}
