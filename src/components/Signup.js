
import React,{useRef,useState} from "react"
import {Button,Form,Card,Alert} from "react-bootstrap"
import {useAuth} from "../Authcontex/Authcontext"
import { Link,useNavigate } from "react-router-dom";




export default function Signup() {
    const EmailRef = useRef();
    const PasswordRef = useRef();
    const passwordcfRef = useRef();
    const { signup } = useAuth();
    const [error,setError] = useState("")
    const [success,setSuccess] = useState("")
    const [loading,setLoading] = useState(false)
    const navigate = useNavigate();


   async function handleSubmit(e){
        e.preventDefault()
       
        if(PasswordRef.current.value !== passwordcfRef.current.value){
            return setError("password don't match")
        }
        try{
            setError("")
            setLoading(true)
            await signup(EmailRef.current.value, PasswordRef.current.value)
            setSuccess("Create an account successfully")
            setTimeout(() => navigate("/dashboard"), 2000)
            
            
        }catch{
            setError("Failed to create an account")
        }
        setLoading(false)
    }

  return (
    <>
    <Card>
       <Card.Body>
        <h2 className='text-center mt-4'>Sign up</h2>
        {success && <Alert variant='success'>{success}</Alert>}
        {error && <Alert variant='danger'>{error}</Alert>}
        <Form onSubmit={handleSubmit}>
            <Form.Group id='email'>
                <Form.Label>Email</Form.Label>
                <Form.Control type='email' ref={EmailRef} required/>
            </Form.Group>

            <Form.Group id='password'>
                <Form.Label>Password</Form.Label>
                <Form.Control type='password' ref={PasswordRef} required/>
            </Form.Group>
           
            <Form.Group id='password-confirm'>
                <Form.Label>Password Confirm</Form.Label>
                <Form.Control type='password' ref={passwordcfRef} required/>
            </Form.Group>
            
            <Button disabled={loading} className='w-100' type='submit'>Sign Up</Button>
        </Form>
       
       </Card.Body>

    </Card>
     <div className='w-100 text-center mt-2'>
        Already have an account? <Link to='/login'>Log in</Link>
     </div>
    </>
   
  )
}
